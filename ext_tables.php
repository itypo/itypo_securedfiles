<?php

use \TYPO3\CMS\Core\Utility\ExtensionManagementUtility;

if (!defined('TYPO3_MODE')) die ('Access denied.');

// Adding the access field to filemounts
$tempColumns = Array (
	'fe_group' => Array (
		'exclude' => 1,
		'label' => 'Access - Don\'t forget to configure your .htaccess file. (Or files will NOT be protected!) (Only works with relative /fileadmin/ as base)',
		'config' => Array (
			'type' => 'select',
			'size' => 15,
			'maxitems' => 99,
			'items' => Array (
				Array('LLL:EXT:lang/locallang_general.php:LGL.hide_at_login', -1),
				Array('LLL:EXT:lang/locallang_general.php:LGL.any_login', -2),
				Array('LLL:EXT:lang/locallang_general.php:LGL.usergroups', '--div--')
			),
			'exclusiveKeys' => '-1,-2',
			'foreign_table' => 'fe_groups',
			'foreign_table_where' => 'ORDER BY fe_groups.title',
		),
	),	
);

ExtensionManagementUtility::addTCAcolumns('sys_filemounts',$tempColumns,1);
ExtensionManagementUtility::addToAllTCAtypes('sys_filemounts','fe_group');

// add static typoscript
ExtensionManagementUtility::addStaticFile($_EXTKEY,'static/itypo_securedfiles/', 'iTypo Secured Files');