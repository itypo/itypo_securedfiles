<?php

/***************************************************************
 * Extension Manager/Repository config file for ext "itypo_securedfiles".
 *
 * Auto generated 31-01-2013 13:42
 *
 * Manual updates:
 * Only the data in the array - everything else is removed by next
 * writing. "version" and "dependencies" must not be touched!
 ***************************************************************/

$EM_CONF[$_EXTKEY] = array(
	'title' => 'iTypo Secured Files',
	'description' => 'Simple protection of files. You can protect filemounts with fe_groups. Requires simple htaccess setting. Supports mod_xsendfile.',
	'category' => 'fe',
	'author' => 'Sander Leeuwesteijn | iTypo',
	'author_email' => 'info@itypo.nl',
	'shy' => '',
	'dependencies' => '',
	'conflicts' => '',
	'priority' => '',
	'module' => '',
	'state' => 'stable',
	'internal' => '',
	'uploadfolder' => 0,
	'createDirs' => '',
	'modify_tables' => '',
	'clearCacheOnLoad' => 0,
	'lockType' => '',
	'author_company' => 'iTypo',
	'version' => '0.0.0',
	'constraints' => array(
		'depends' => array(
			'typo3' => '6.2.12-6.2.99',
		),
		'conflicts' => array(
		),
		'suggests' => array(
		),
	),
	'_md5_values_when_last_written' => 'a:9:{s:22:"class.securedfiles.php";s:4:"05cd";s:21:"ext_conf_template.txt";s:4:"cbdc";s:12:"ext_icon.gif";s:4:"6cd0";s:17:"ext_localconf.php";s:4:"71d1";s:14:"ext_tables.php";s:4:"d5ed";s:14:"ext_tables.sql";s:4:"695d";s:14:"doc/manual.sxw";s:4:"3831";s:39:"static/itypo_securedfiles/constants.txt";s:4:"c71a";s:35:"static/itypo_securedfiles/setup.txt";s:4:"ad98";}',
	'suggests' => array(
	),
);