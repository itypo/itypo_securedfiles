﻿<?php

use \TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * eID which checks if you are logged in and have the correct groups, if so gives access to the requested file. (set in filemounts)
 * File has to be one of the allowed extensions.
 * - ONLY FOR FILEMOUNTS WITH RELATIVE PATH (which is fileadmin by default)
 *
 * @author Sander Leeuwesteijn | iTypo <info@itypo.nl>
 */
 
class tx_securedfiles {

    /**
     * Will hold all groups and subgroups of the currently logged in user.
     * @var array
     */
    private $groups = array();

    /**
     * Default outputEngine.
     * @var string
     */
    private $outputEngine = 'readfile';

    /**
     * Default list of allowed extensions.
     * @var string
     */
    private $allowedExtensions = 'pdf,zip,jpg,jpeg,png,gif,xls,xlsx,doc,docx,rar,7z,tar,gz,exe,bmp,txt,odt,odf,rtf,htm,html,csv,pps,ppt,pptx,xml,wav,mp3,wma,avi,wmv,swf,flv,mp4,mpg,mov,tif,psd,eps,bin,iso,dmg,msi';

    /**
     * Holds the GET params.
     * @var array
     */
    private $get = array();

    /**
     * Holds the extension config.
     * @var string
     */
    private $extConf;
    
    /** 
     * Database access functions
     * @var t3lib_db
     */
    private $db;

    /**
     * Holds the cObj.
     * @var tslib_cObj
     */
    private $cObj;

    /**
     * This function is executed on eID call.
     */
    function main() {
		// get some basic config
        $this->db = $GLOBALS['TYPO3_DB'];
		$this->get = GeneralUtility::_GET();
		$this->extConf = unserialize($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf']['itypo_securedfiles']);
		$this->allowedExtensions = $this->extConf['allowedFileExtensions'] ? explode(',',$this->extConf['allowedFileExtensions']) : explode(',',$this->allowedExtensions);
        $this->outputEngine = $this->extConf['outputEngine'] ? $this->extConf['outputEngine'] : $this->allowedExtensions;
		$pathInfo = pathinfo($this->get['file']);

        // check if were able to output
        if ($this->outputEngine == 'mod_xsendfile' && !in_array('mod_xsendfile', apache_get_modules())) {
            throw new \TYPO3\CMS\Core\Exception('Module mod_xsendfile selected as output engine but the module is not loaded in Apache!');
        }

		if (isset($this->get['file']) && in_array(strtolower($pathInfo['extension']),$this->allowedExtensions)) {
			$feUserObj = \TYPO3\CMS\Frontend\Utility\EidUtility::initFeUser();
			
			// user is not logged in, so redirect to login page
			if (!$feUserObj->user['uid']) {
				// create regular FE environment so we can link, access setup, etc
				$this->createTSFE();

				// support for itypo_404handler (takes precedence)
				if (\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::isLoaded('itypo_404handler')) {
					$setup = $GLOBALS['TSFE']->tmpl->setup['plugin.']['itypo_404handler.'];				
				} else {
					$setup = $GLOBALS['TSFE']->tmpl->setup['plugin.']['tx_ityposecuredfiles.'];
				}
				
				// set loginpage
				if (is_numeric($setup['loginPage'])) {
					$loginPage = $this->cObj->getTypoLink_URL(intval($setup['loginPage']));
				} else {
					$loginPage = $setup['loginPage'];
				}
				
				// if the url ends with a /, add a ? for the parameters, this happens when realurl is used for example
				if (substr($loginPage,-1,1) == '/') $loginPage .= '?';

                \TYPO3\CMS\Core\Utility\HttpUtility::redirect($loginPage.'&redirect_url='.$this->get['file']);
			} else {
				// fetch the filemounts which are protected by a group (only with relative base)
				$res = $this->db->exec_SELECTquery('path,fe_group','sys_filemounts',"base = 1 AND fe_group != 0 AND hidden = 0 AND deleted = 0");
				
				if ($res && $this->db->sql_num_rows($res) > 0) {
                    $filemounts = array();
					while ($row = $this->db->sql_fetch_assoc($res)) {
						// array with all filemounts
						$filemounts[] = $row;
					}

					$userGroups = $this->getUsergroups($feUserObj);					// the fe_users groups
					$requestedDir = substr($pathInfo['dirname'],10).'/'; 			// remove /fileadmin/ part and add trailing slash
					$numberOfSubDirs = count(explode('/',$requestedDir)) - 1;		// minus 1 for the trailing slash added above..
					
					// build a recursive list of directories
					$dirs[] = $requestedDir;
					for ($i = 1; $i < $numberOfSubDirs; $i++) {
						$dirs[] = dirname($requestedDir).'/';						// add trailing slash
						$requestedDir = dirname($requestedDir);
					}

					// loop all directories and find the matching filemounts
					foreach ($dirs as $dir) {
						if ($groups = $this->array_multi_search($dir,$filemounts)) {
							// user should be a member of one of these groups belonging to this filemount, otherwise we can stop now
							if (count(array_intersect($userGroups,$groups)) == 0) {
								$this->throw403();
							}
						}
					}
				}

                // we were not stopped by a 403, we can display the file now.
                $this->fileOutput($this->get['file']);
			}
		} else {
			$this->throw404();
		}
	}
	
	/**
	 * Gets all main fe_groups and subgroups for a user
	 *
	 * @param	object		$feUserObj		Complete fe_user object
	 * @return	array		$this->groups	Full array of this users main groups and subgroups
	 */	
	private function getUsergroups($feUserObj) {
		$this->getSubGroups($feUserObj->user['usergroup']);
		$this->groups[] = '-2';	// append -2 which means any logged in user
		return $this->groups;
	}
	
	/**
	 * Fetches subgroups of groups. Function is called recursively for each subgroup.
	 *
	 * @param	string		$grList Commalist of fe_groups uid numbers
	 * @param	string		$idList List of already processed fe_groups-uids so the function will not fall into a eternal recursion.
	 * @return	void
	 */
	private function getSubGroups($grList,$idList='')	{
		$res = $this->db->exec_SELECTquery('uid,subgroup', 'fe_groups', 'deleted=0 AND hidden=0 AND uid IN ('.$grList.')');
		
		// Internal group record storage
		$groupRows = array();

		// The groups array is filled
		while ($row = $this->db->sql_fetch_assoc($res))	{
			if(!in_array($row['uid'], $this->groups))	{ $this->groups[] = $row['uid']; }
			$groupRows[$row['uid']] = $row;
		}

		// Traversing records in the correct order
		$include_staticArr = GeneralUtility::intExplode(',', $grList);
		foreach($include_staticArr as $uid)	{	// traversing list
			// Get row:
			$row=$groupRows[$uid];
			if (is_array($row) && !GeneralUtility::inList($idList,$uid))	{	// Must be an array and $uid should not be in the idList, because then it is somewhere previously in the grouplist

				// Include subgroups
				if (trim($row['subgroup']))	{
					$theList = implode(',',GeneralUtility::intExplode(',',$row['subgroup']));	// Make integer list
					$this->getSubGroups($theList, $idList.','.$uid);	// Call recursively, pass along list of already processed groups so they are not recursed again.
				}
			}
		}
	}
	
	/**
	 * Searches for a string inside a two level array.
	 * Used to find a filemount with a matching path.
	 *
	 * @param	string	$str	String to search for.
	 * @param	array	$array	Array to search in.
	 * @return	array when match or false.
	 */
	private function array_multi_search($str, $array) {
		foreach ($array as $key => $val) {
			if ($val['path'] == $str) {
				return explode(',',$val['fe_group']);
			}
		}
		return false;
	}
	
	/**
	 * Outputs the requested file, if it exists.
	 *
	 * @param	string	$inputFile	The path to the file, for example: fileadmin/protec/test.pdf
	 * @return	void
	 */
	protected function fileOutput($inputFile){
		$file = GeneralUtility::getFileAbsFileName(ltrim($inputFile, '/'));

		// This is a workaround for a PHP bug on Windows systems:
		// @see http://bugs.php.net/bug.php?id=46990
		// It helps for filenames with special characters that are present in latin1 encoding.
		// If you have real UTF-8 filenames, use a nix based OS.
		// NOTE: needs to be checked, if the website encoding really is UTF-8 and if UTF-8 filesystem is enabled
		if (TYPO3_OS == 'WIN') {
			$file = utf8_decode($file);
		}

		if (file_exists($file)) {
			// get mimetype
			$finfo = finfo_open(FILEINFO_MIME_TYPE);
			$mimetype = finfo_file($finfo, $file);
			finfo_close($finfo);

            // end transaction when downloading is about to start
            if (extension_loaded('newrelic')) {
                /** @noinspection PhpUndefinedFunctionInspection */
                newrelic_end_of_transaction();
            }

			header('Pragma: private');
			header('Expires: 0');
			header('Cache-Control: must-revalidate');
			header('Content-Type: ' . $mimetype);
			header('Content-Disposition: inline; filename="' . basename($file) . '"');

            // determine how and push the file to the client
            if ($this->outputEngine == 'mod_xsendfile') {
                header('X-Sendfile: ' . $file);
            } elseif ($this->outputEngine == 'readfile_chunked') {
                header('Content-Length: ' . filesize($file));
                ob_clean();
                flush();
                $this->readfile_chunked($file);
            } else {
                header('Content-Length: ' . filesize($file));
                ob_clean();
                flush();
                readfile($file);
            }

			exit;
		} else {
			$this->throw404();
		}
	}

    /**
     * Initializes TSFE. This is necessary to have proper environment for typoLink.
     * This function is based on the one from extension 'pagepath' by Dmitry Dulepov.
     *
     * @return	void
     */
    protected function createTSFE() {
        $GLOBALS['TSFE'] = GeneralUtility::makeInstance('tslib_fe');

        $GLOBALS['TSFE']->connectToDB();
        $GLOBALS['TSFE']->initFEuser();
        $GLOBALS['TSFE']->determineId();
        $GLOBALS['TSFE']->getCompressedTCarray();
        $GLOBALS['TSFE']->initTemplate();
        $GLOBALS['TSFE']->getConfigArray();

        $this->cObj = GeneralUtility::makeInstance('tslib_cObj');

        // Set linkVars, absRefPrefix, etc
        \TYPO3\CMS\Frontend\Page\PageGenerator::pagegenInit();
    }

    /**
     * Throw a 404 not found error.
     */
    protected function throw404() {
		$GLOBALS['TSFE'] = GeneralUtility::makeInstance('tslib_fe');
		$GLOBALS['TSFE']->initFEuser();
		$GLOBALS['TSFE']->pageNotFoundHandler($GLOBALS['TYPO3_CONF_VARS']['FE']['pageNotFound_handling'],$GLOBALS['TYPO3_CONF_VARS']['FE']['pageNotFound_handling_statheader'],'File does not exist');
	}

    /**
     * Throw a 403 forbidden error.
     */
    protected function throw403() {
		$GLOBALS['TSFE'] = GeneralUtility::makeInstance('tslib_fe');
		$GLOBALS['TSFE']->initFEuser();
		$GLOBALS['TSFE']->pageNotFoundHandler($GLOBALS['TYPO3_CONF_VARS']['FE']['pageNotFound_handling'],'HTTP/1.0 403 Forbidden','No permission to access this file');
	}

    /**
     * In some cases php needs the filesize as php_memory, so big files cannot
     * be transferred. This function mitigates this problem.
     *
     * This function is based on the one from extension 'naw_securedl' by Dietrich Heise.
     *
     * @param string $strFileName
     * @return bool
     */
    protected function readfile_chunked($strFileName) {
        $chunksize = 2048000; // how many bytes per chunk
        $timeout = ini_get('max_execution_time');
        $bytes_sent = 0;
        $handle = fopen($strFileName, 'rb');
        if ($handle === false) {
            return false;
        }
        while (!feof($handle) && (!connection_aborted()) ) {
            set_time_limit($timeout);
            $buffer = fread($handle, $chunksize);
            print $buffer;
            $bytes_sent += $chunksize;
            ob_flush();
            flush();
        }
        return fclose($handle);
    }
}

$securedfiles = GeneralUtility::makeInstance('tx_securedfiles');
echo $securedfiles->main();